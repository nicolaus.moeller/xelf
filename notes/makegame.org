#+TITLE: CL-MAKEGAME

* TODO Reduce boilerplate and cleanup
** TODO [#A] simplify projects path  handling

** TODO [#C] rename *resizable* to *window-resizable-p*?

** TODO [#A] Refactor collision detection to enable various types.
*** delegate actual collision detection to method of the object that is moving (not collidee, who hasn't been identified)
*** in cell/move and sprite/move , compute whether a move would cause a collision
*** and then only allow the move to go a certain distance. return number moved
*** then "resting contact" will work; the object won't try to move anymore (if gravity.)

** TODO properties browser with COMMAND.LISP and emacs-Customize style specs
** TODO optional support for tiling and grids, tileset/anim import, collision-bound shape selector and optional procgen tags
** TODO edit in gimp


* TODO Design complete menu/command set and M-x "Lisp:" thing
* TODO Support CUA-mode?
* TODO Investigate libterm
** TODO NOTES on integrating emacs
<dto> mood: thanks. Baggers` what do you do in CEPL to keep the repl running
      from inside the game loop?  [11:34]
<dto> i think i remember something about such code, but it would help me if
      you could explain
<Baggers`> dto: https://github.com/cbaggers/swank.live it's in quicklisp too
								        [11:35]
<dto> oh. nice !
<Baggers`> dto: also more experimental is livesupport which is also in QL and
	   is meant to support slime and sly  [11:36]
<dto> i'll give swank.live a shot first, thanks so much for sharing this. it
      seems easy enough to drop in 
<dto> do you know if it works on Windows?
<Baggers`> dto: it does
<dto> cool :)  [11:37]
<Baggers`> dto: also, if you get to the point where you want to use slime in
	   single threaded mode (so you have your repl and C-c C-k happen in
	   the same thread) you can use this
	   https://gist.github.com/cbaggers/592e97c64d37e58d80f09c2b365b161c
<Baggers`> add to your .emacs file and call slime-style to specify the
	   communication style for swank to use  [11:38]

https://github.com/3b/3bst  [22:26]
<dto> |3b|: i may very well use this. it seems like it would be
      straightforward to render the term  [22:34]
<dto> theoretically i could start emacs as a subprocess and have its slime
      communicate with the current swank  [22:35]
<dto> optionally, emacsclient could open a terminal frame from within the
      (possibly graphical) emacs that spawned the SBCL  [22:36]

  
* Install helper
** Help installing Lisp, Quicklisp, and Emacs on your platform
*** Choose CUA-mode and other emacs options
** Choose where startup file goes
** Choose default projects folder 
* Project helper
** User fills in required info (author, project name, title etc)
** Create a new directory called projectname.xelf
*** Can compress for distribution into .xelf.gz
** Auto-written package.lisp and projectname.asd
** Auto-launch scripts, auto-executable dump
** Optional license chooser in LICENSE.txt
** Auto very simple boilerplate yasnippets / template-mode
** Starter kits with basic classes/assets
*** Simple pong
*** Simple shooter
*** Simple adventure
*** Basic physics
** Fork/snapshot/merge projects for crazy remixing/improvisation/jamming
* Class browser
** Creating a new class optionally creates classname.lisp and updates the .asd
* Resource browser
** Change volume properties of sounds/music 
** Customize available font sets
** Possible emacs image-dired and other imagemagick
* Buffer editor/manager
* Game test

* Disorganized tasks section
** DONE gather all xelf code from /obsolete/dir and /alpha and glass.el
   CLOSED: [2016-04-11 Mon 18:30]
** DONE review blocks.lisp
   CLOSED: [2016-04-11 Mon 18:30]
** TODO review buffers.lisp drag/drop code
** TODO revise modeline/dialog box classes; revive DEFINE-BLOCK-MACRO
** TODO revise halos
** TODO possibly rename PHRASE class

** TODO design a complete workflow
*** TODO use quickproject? to make projects and do everything properly when creating a CL-MAKEGAME
*** TODO save system state using cypress-style serialization to projectname.xelf
*** TODO snapshot / fork projects
*** TODO ship a windows CUA-mode option? 
** TODO identify code revisions
** TODO need ready made classes and several example games to build on
