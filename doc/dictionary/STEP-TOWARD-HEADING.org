#+OPTIONS: *:nil
** STEP-TOWARD-HEADING (generic function)
 
: (NODE HEADING &OPTIONAL (DISTANCE))
Return as multiple values the point DISTANCE units
at angle HEADING away from the center of NODE. 
