#+OPTIONS: *:nil
** CURRENT-DIRECTORY (function)
Returns the pathname of the current directory.
