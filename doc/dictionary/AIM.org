#+OPTIONS: *:nil
** AIM (generic function)
 
: (NODE HEADING)
Set the NODE's current heading to HEADING.
