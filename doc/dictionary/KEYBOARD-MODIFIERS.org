#+OPTIONS: *:nil
** KEYBOARD-MODIFIERS (function)
Returns a list of the modifier keys that are depressed.
