#+OPTIONS: *:nil
** FOLLOW-WITH-CAMERA (generic function)
 
: (BUFFER NODE)
Arrange for the BUFFER to follow NODE with the camera as it moves.
