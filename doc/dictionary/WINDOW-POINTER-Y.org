#+OPTIONS: *:nil
** WINDOW-POINTER-Y (function)
 
: (&OPTIONAL (Y *POINTER-Y*))
Return the absolute y-coordinate of the mouse pointer.
