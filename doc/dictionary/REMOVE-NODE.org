#+OPTIONS: *:nil
** REMOVE-NODE (generic function)
 
: (BUFFER NODE)
Remove the object NODE from BUFFER.
