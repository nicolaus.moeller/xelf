#+OPTIONS: *:nil
** DO-NODES (macro)
 
: (SPEC &BODY BODY)
Iterate over the nodes in BUFFER, binding VAR to each node and
evaluating the forms in BODY for each. SPEC is of the form (VAR
BUFFER).
