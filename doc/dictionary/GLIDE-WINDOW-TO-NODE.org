#+OPTIONS: *:nil
** GLIDE-WINDOW-TO-NODE (generic function)
 
: (BUFFER NODE)
Configure window to glide smoothly to the NODE.
