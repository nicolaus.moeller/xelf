#+OPTIONS: *:nil
** *FULLSCREEN* (variable)
When non-nil, attempt to use fullscreen
mode.
