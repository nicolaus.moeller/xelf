#+OPTIONS: *:nil
** Y (generic function)
 
: (NODE)
Return the current y-coordinate of the NODE.
