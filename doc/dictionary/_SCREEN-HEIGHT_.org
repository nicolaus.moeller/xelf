#+OPTIONS: *:nil
** *SCREEN-HEIGHT* (variable)
Physical height of the window, in pixels.
