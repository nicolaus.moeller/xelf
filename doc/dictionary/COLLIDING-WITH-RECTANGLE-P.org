#+OPTIONS: *:nil
** COLLIDING-WITH-RECTANGLE-P (generic function)
 
: (NODE TOP LEFT WIDTH HEIGHT)
Return non-nil if NODE is colliding with the given rectangle.
