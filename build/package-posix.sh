#!/bin/sh

GAME=1x0ng
VERSION=0.5

cd ~/release
git clone https://github.com/dto/xelf.git
mv xelf 1x0ng-linux-0.6
cd 1x0ng-linux-0.6
git clone https://github.com/dto/1x0ng.git
rm -rf .git
rm -rf 1x0ng/.git
cp ~/xelf/1x0ng.bin .
cd ~/release
tar cvzf 1x0ng-linux-0.6.tar.gz 1x0ng-linux-0.6

cp -R 1x0ng-linux-0.6 1x0ng-windows-0.6
cd 1x0ng-windows-0.6
rm *.bin
cp ~/xelf/1x0ng.exe .
cp ~/xelf/*.dll .
cd ..
zip -r 1x0ng-windows-0.6.zip 1x0ng-windows-0.6


