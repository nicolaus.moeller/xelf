LISPDIR=/usr/local/bin
LISP=$LISPDIR/sbcl

NAME="2fr0g"

rm -rf $NAME.app
$LISP --no-sysinit --no-userinit --load bootslime.lisp

cp -a Frameworks $NAME.app/Contents/
mkdir -p $NAME.app/Contents/Resources/projects/2fr0g
cp -a xelf/standard $NAME.app/Contents/Resources/projects/
cp 2fr0g/COPYING 2fr0g/*.{wav,png,ogg,txt,ttf,otf} $NAME.app/Contents/Resources/projects/2fr0g
mkdir -p $NAME.app/Contents/Source
cp 2fr0g/*.lisp 2fr0g/*.asd xelf/*.lisp xelf/*.asd $NAME.app/Contents/Source
mkdir -p $NAME.app/Contents/Licenses
cp xelf/licenses/* $NAME.app/Contents/Licenses

