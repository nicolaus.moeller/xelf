These instructions are for building 32-bit Mac OSX apps with Xelf and
SBCL. You will need the git command line tools, via Xcode or
elsewhere.  The repo includes the 32-bit binary dylib frameworks for
SDL and LISPBUILDER-SDL. The script BOOTSLIME.LISP is presently
somewhat ugly, but can be adapted for your purpose even if you don't
use Xelf or SDL.

1. Download the newest stable x86-darwin build of SBCL from sbcl.org.
2. Install SBCL by extracting and running "sudo sh install.sh" in the resulting dir.
3. Download the newest stable source code of SBCL.
4. Extract the SBCL source and run the following command in that directory:

    sh make.sh --arch=x86 --with-sb-thread

5. Install this newly compiled SBCL by doing:

    sudo sh install.sh

6. Clone the staging area repo with its build scripts:

    git clone http://gitlab.com/dto/delivery.git

7. CD to the ~/delivery repo and clone these inside the staging area:

    git clone http://gitlab.com/dto/xelf.git
    git clone http://gitlab.com/dto/quadrille.git
    git clone http://gitlab.com/dto/inv8r.git      (or any other xelf game)

8. Install quicklisp by downloading

    https://beta.quicklisp.org/quicklisp.lisp

   And running it with SBCL:

    sbcl --load quicklisp.lisp

9. Symlink the xelf, inv8r, and quadrille repos into
   ~/quicklisp/local-projects

10.  Edit bootslime.lisp and deliver.sh to reflect the app name.

11. Run deliver.sh

12. You should have a working "inv8r.app". Try running it with "open
inv8r.app" at the terminal.

13. Use the Mac Disk Utility to make your .app into a nice .dmg
installer for distribution.
